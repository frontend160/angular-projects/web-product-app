import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, map, Observable, of, startWith } from 'rxjs';
import { ProductsService } from 'src/app/services/products.service';
import { AppDataState, dataStateEnum } from 'src/app/state/product.state';
import { Product } from '../../model/product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {

  products$: Observable<AppDataState<Product[]>> | null = null;
  readonly DataStateEnum = dataStateEnum;

  constructor(private productsService: ProductsService, private router: Router) {}

  ngOnInit(): void {}

  onGetAllProducts() {
    this.products$ = this.productsService.getAllProducts().pipe(
      map((data) => {
        return ({ dataState: dataStateEnum.LOADED, data: data })
      }),
      startWith({ dataState: dataStateEnum.LOADING }),
      catchError((err) =>
        of({ dataState: dataStateEnum.ERROR, errorMessage: err.message })
      )
    );
  }

  onGetSelectedProducts() {
    this.products$ = this.productsService.getSelectedProducts().pipe(
      map((data) => {
        return ({ dataState: dataStateEnum.LOADED, data: data })
      }),
      startWith({ dataState: dataStateEnum.LOADING }),
      catchError((err) =>
        of({ dataState: dataStateEnum.ERROR, errorMessage: err.message })
      )
    );
  }

  onGetAvailableProducts() {
    this.products$ = this.productsService.getAvailableProducts().pipe(
      map((data) => {
        return ({ dataState: dataStateEnum.LOADED, data: data })
      }),
      startWith({ dataState: dataStateEnum.LOADING }),
      catchError((err) =>
        of({ dataState: dataStateEnum.ERROR, errorMessage: err.message })
      )
    );
  }

  onSearch(dataForm: { keyword: string; }){
    this.products$ = this.productsService.getSearchedProducts(dataForm.keyword).pipe(
      map((data) => {
        return ({ dataState: dataStateEnum.LOADED, data: data })
      }),
      startWith({ dataState: dataStateEnum.LOADING }),
      catchError((err) =>
        of({ dataState: dataStateEnum.ERROR, errorMessage: err.message })
      )
    );
  }

  onSelect(product: Product) {
    this.productsService.selectProduct(product).subscribe(data => {
      product.selected = data.selected;
    })
  }

  onDelete(product: Product) {
    this.productsService.deleteProduct(product).subscribe(data => {
      this.onGetAllProducts();
    })
  }

  onNewProduct() {
    this.router.navigateByUrl("/add-product");
  }

  onEditProduct(product: Product) {
    this.router.navigateByUrl("/edit-product/"+ product.id);
  }
}
